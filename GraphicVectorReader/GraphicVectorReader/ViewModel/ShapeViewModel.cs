﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows;

namespace GraphicVectorReader.ViewModel
{
    public abstract class ShapeViewModel : ViewModelBase
    {
        #region Поля
        protected double _top;
        protected double _left;
        private bool _selected;
        protected ShapesCollectionViewModel _collection;
        #endregion  //Поля

        #region Конструкторы
        public ShapeViewModel(ShapesCollectionViewModel collection)
        {
            _collection = collection;
            _collection.Shapes.Add(this);
            _collection.PropertyChanged += _collection_PropertyChanged;
        }
        #endregion  //Конструкторы

        #region Свойства
        /// <summary>
        /// Позиция по Х
        /// </summary>
        public double Top
        {
            get { return _top + _collection.TopCommonCenter; }
            set
            {
                if (_top != value )
                {

                    if (double.IsNaN(value))
                    { 
                    }
                    if (_collection.Shapes.Count(s => s.Selected) > 1)
                    {
                        double increment = value - RealTop;
                        foreach (ShapeViewModel shape in _collection.Shapes)
                        {
                            if (shape.Selected == true)
                            {
                                shape.UpdateTop(increment);
                            }
                        }
                    }
                    else if (this is SegmentViewModel)
                    {
                        SegmentViewModel cur = this as SegmentViewModel;
                        if (cur.IsInicialized == false)
                        {
                            cur.FullLineUpdateTop(value - RealTop);
                        }
                        else
                        {
                            _top = value;
                        }
                    }
                    else
                    {
                        _top = value;
                    }
                    OnPropertyChanged("Top");
                    OnPropertyChanged("RealTop");
                }
            }
        }

        public double RealTop
        {
            get { return _top; }
        }

        /// <summary>
        /// Позиция по Y
        /// </summary>
        public double Left
        {
            get { return _left + _collection.LeftCommonCenter; }
            set
            {
                if (_left != value)
                {
                    if (double.IsNaN(value))
                    {
                    }
                    if (_collection.Shapes.Count(s => s.Selected) > 1)
                    {
                        double increment = value - RealLeft;
                        foreach (ShapeViewModel shape in _collection.Shapes)
                        {
                            if (shape.Selected == true)
                            {
                                shape.UpdateLeft(increment);
                            }
                        }
                    }
                    else if (this is SegmentViewModel)
                    {
                        SegmentViewModel cur = this as SegmentViewModel;
                        if (cur.IsInicialized == false)
                        {
                            cur.FullLineUpdateLeft(value - RealLeft);
                        }
                        else
                        {
                            _left = value;
                        }
                    }
                    else
                    {
                        _left = value;
                    }
                    OnPropertyChanged("Left");
                    OnPropertyChanged("RealLeft");
                }
            }
        }

        public double RealLeft
        {
            get { return _left; }
        }

        /// <summary>
        /// Признак того что элемент выбран
        /// </summary>
        public bool Selected
        {
            get { return _selected; }
            set
            {
                if (_selected != value)
                {
                    _selected = value;
                    if (value)
                    {
                        _collection.SelectedShape = this;
                    }
                    OnPropertyChanged("Selected");
                    
                }
            }
        }
        #endregion  //Свойства

        #region Методы
        public virtual bool IncludeInRegion(double x1, double y1, double x2, double y2)
        { return false; }

        public virtual void UpdateLeft(double incrementLeft)
        {
            _left += incrementLeft;
            OnPropertyChanged("Left");
            OnPropertyChanged("RealLeft");
        }

        public virtual void UpdateTop(double incrementTop)
        {
            _top += incrementTop;
            OnPropertyChanged("Top");
            OnPropertyChanged("RealTop");
        }

        void _collection_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Top" || e.PropertyName == "RealTop" || e.PropertyName == "TopCommonCenter")
            {
                OnPropertyChanged("Top");
                OnPropertyChanged("RealTop");
                
            }
            if (e.PropertyName == "Left" || e.PropertyName == "RealLeft" || e.PropertyName == "LeftCommonCenter")
            {
                OnPropertyChanged("Left");
                OnPropertyChanged("RealLeft");
            }
        }
        #endregion
    }
}

