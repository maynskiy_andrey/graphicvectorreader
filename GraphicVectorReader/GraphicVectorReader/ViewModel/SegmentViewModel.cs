﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;

namespace GraphicVectorReader.ViewModel
{
    public class SegmentViewModel: ShapeViewModel
    {
        #region Поля и константы
        double defaultWidth = 12;
        double minRadius = 6;
        double compoundingFactorRadius = 1.5;

        double defaulLeftNodeLeft = 10;
        double defaulLeftNodeTop = 50;
        double defaulRightNodeLeft = 70;
        double defaulRightNodeTop = 50;
        Color defaultColor = Colors.Black;

        NodeViewModel _rightNode;
        NodeViewModel _leftNode;
        SegmentViewModel _nextSegment;
        SegmentViewModel _prevSegment;
        double _width;
        double _heigth;
        double _angle;
        private Color _color;
        ICommand _createNewSegmentCommand;
        bool isInicialized = false;
        private bool innerUpdateParameter = false;
        #endregion  //Поля и константы

        #region Конструкторы
        public SegmentViewModel(ShapesCollectionViewModel collection)
            : base(collection)
        {
            isInicialized = false;
            innerUpdateParameter = true;
            LeftNode = new NodeViewModel(collection);
            LeftNode.Segment = this;
            LeftNode.PropertyChanged += LeftNode_PropertyChanged;
            LeftNode.Left = defaulLeftNodeLeft - _collection.LeftCommonCenter;
            LeftNode.Top = defaulLeftNodeTop - _collection.TopCommonCenter;
            RightNode = new NodeViewModel(collection);
            RightNode.Segment = this;
            RightNode.PropertyChanged += LeftNode_PropertyChanged;
            RightNode.Left = defaulRightNodeLeft - _collection.LeftCommonCenter;
            RightNode.Top = defaulRightNodeTop - _collection.TopCommonCenter;
            WidthControl = defaultWidth;
            this.Color = defaultColor;
            isInicialized = true;
            Selected = true;
            innerUpdateParameter = false;
            CalculateRib();
        }
        
        public SegmentViewModel(SegmentViewModel oldSegment, ShapesCollectionViewModel collection)
            : base(collection)
        {
            isInicialized = false;
            innerUpdateParameter = true;
            Vector v1 = new Vector(oldSegment.RightNode.Left - oldSegment.LeftNode.Left, oldSegment.RightNode.Top - oldSegment.LeftNode.Top);
            v1 = Vector.Divide(v1, 2);
            Point newPoint = v1 + new Point(oldSegment.LeftNode.RealLeft, oldSegment.LeftNode.RealTop);
            PrevSegment = oldSegment;

            RightNode = new NodeViewModel(collection);
            RightNode.Segment = this;
            RightNode.PropertyChanged += LeftNode_PropertyChanged;
            RightNode.Left = oldSegment.RightNode.RealLeft;
            RightNode.Top = oldSegment.RightNode.RealTop;

            oldSegment.RightNode.Left = newPoint.X;
            oldSegment.RightNode.Top = newPoint.Y;

            LeftNode = oldSegment.RightNode;
            LeftNode.PropertyChanged += LeftNode_PropertyChanged;
            WidthControl = oldSegment.WidthControl;



            if (oldSegment.NextSegment != null)
            {
                NextSegment = oldSegment.NextSegment;
                oldSegment.NextSegment = null;
                NextSegment.PrevSegment = null;
                NextSegment.PrevSegment = this;
                NextSegment.LeftNode = null;
                NextSegment.LeftNode = RightNode;
            }

            this.Color = oldSegment.Color;

            oldSegment.NextSegment = this;
            isInicialized = true;
            innerUpdateParameter = false;
            Selected = true;
            CalculateRib();
        }

        public SegmentViewModel(SegmentViewModel oldSegment, NodeViewModel rightNode, ShapesCollectionViewModel collection)
            : base(collection)
        {
            isInicialized = false;
            innerUpdateParameter = false;
            innerUpdateParameter = false;
            PrevSegment = oldSegment;

            RightNode = rightNode;
            RightNode.PropertyChanged += LeftNode_PropertyChanged;

            LeftNode = oldSegment.RightNode;
            LeftNode.PropertyChanged += LeftNode_PropertyChanged;
            WidthControl = oldSegment.WidthControl;
            this.Color = oldSegment.Color;

            oldSegment.NextSegment = this;
            isInicialized = true;
            innerUpdateParameter = true;
            CalculateRib();
        }
        #endregion  //Конструкторы

        #region Свойства
        public SegmentViewModel NextSegment
        {
            get { return _nextSegment; }
            set
            {
                if (_nextSegment != value)
                {
                    _nextSegment = value;
                    OnPropertyChanged("NextSegment");
                }
            }
        }

        public SegmentViewModel PrevSegment
        {
            get { return _prevSegment; }
            set
            {
                if (_prevSegment != value)
                {
                    _prevSegment = value;
                    OnPropertyChanged("PrevSegment");
                }
            }
        }
        
        public NodeViewModel RightNode
        {
            get { return _rightNode; }
            set
            {
                if (_rightNode != value)
                {
                    if (_rightNode != null)
                    _rightNode.PropertyChanged -= LeftNode_PropertyChanged;
                    _rightNode = value;
                    OnPropertyChanged("RightNode");
                    if (_rightNode != null)
                    {
                        _rightNode.PropertyChanged += LeftNode_PropertyChanged;
                        CalculateRib();
                    }
                }
            }
        }

        public NodeViewModel LeftNode
        {
            get { return _leftNode; }
            set
            {
                if (_leftNode != value)
                {
                    if (_leftNode != null)
                    _leftNode.PropertyChanged -= LeftNode_PropertyChanged;
                    _leftNode = value;
                    OnPropertyChanged("LeftNode");
                    if (_leftNode != null)
                    {
                        _leftNode.PropertyChanged += LeftNode_PropertyChanged;
                        CalculateRib();
                    }
                }
            }
        }

        int _zIndex = 0;
        public int ZIndex
        {
            get { return _zIndex; }
            set
            {
                if (_zIndex != value)
                {
                    _zIndex = value;
                    OnPropertyChanged("ZIndex");
                }
            }
        }

        public double WidthControl
        {
            get { return _width; }
            set
            {
                if (_width != value)
                {
                    _width = value;
                    LeftNode.RadiusControl = RightNode.RadiusControl = minRadius + _width * compoundingFactorRadius;
                    if (PrevSegment != null)
                    {
                        PrevSegment.WidthControl = value;
                    }
                    if (NextSegment != null)
                    {
                        NextSegment.WidthControl = value;
                    }
                    OnPropertyChanged("CenterX");
                    OnPropertyChanged("WidthControl");
                    CalculateRib();
                }
            }
        }

        public double HeightControl
        {
            get { return _heigth; }
            set
            {
                if (_heigth != value)
                {
                    _heigth = value;
                    OnPropertyChanged("CenterY");
                    OnPropertyChanged("HeightControl");
                }
            }
        }

        public double Angle
        {
            get { return _angle; }
            set
            {
                if (_angle != value)
                {
                    _angle = value;
                    OnPropertyChanged("Angle");
                }
            }
        }

        public Color Color
        {
            get { return _color; }
            set
            {
                if (_color != value)
                {
                    _color = value;
                    OnPropertyChanged("Color");
                    if(PrevSegment != null)
                    {
                        PrevSegment.Color = value;
                    }
                    if (NextSegment != null)
                    {
                        NextSegment.Color = value;
                    }
                }
            }
        }

        public double CenterY
        {
            get { return _width / 2;}
        }


        public bool IsInicialized
        {
            get { return innerUpdateParameter; }
        }
        #endregion  //Свойства

        #region Команды
        public ICommand CreateNewSegmentCommand
        {
            get
            {
                if (_createNewSegmentCommand == null)
                    _createNewSegmentCommand = new RelayCommand(param => this.CreateNewSegment());

                return _createNewSegmentCommand;
            }
        }
        #endregion  //Команды

        #region Методы
        void CreateNewSegment()
        {
            SegmentViewModel newSegment = new SegmentViewModel(this, _collection);
        }

        void LeftNode_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (innerUpdateParameter == true)
            {
                return;
            }
            //для запуска расчета нужно что бы
            if (isInicialized &&    //сегмент был инициирован
                innerUpdateParameter == false &&    //не проходило внутренних изменений
                (e.PropertyName == "Top" || e.PropertyName == "Left")      //обновляемое свойсто относилось к координатам)
                  )//|| RightNode.IsMoving == false && LeftNode.IsMoving == false  )       //или если движение остановилось (для упраащения удаления ребер)
            {
                CalculateRib();
            }
          
            OnPropertyChanged("CenterY");
        }

        void CalculateRib()
        {
           
            if (isInicialized == false || RightNode == null || LeftNode == null)      //расчет прерывается если один из узлов отсутвует
            {
                return;
            }
            if (Selected == true && RightNode.Selected == true && LeftNode.Selected == true     //расчет прерывается если все элементы выбраны (а значит идет массовое перемещение)
              || Selected == false && RightNode.Selected == false && LeftNode.Selected == false)        //или если все элементы не выбраны ( а значит происходит изменение центра рабочей поверхности)
            {
                return;
            }
            if (isInicialized && RightNode.IsMoving == false && LeftNode.IsMoving == false)
            {
                #region Рассчет удаления ребер
                if (Math.Abs(RightNode.RealLeft - LeftNode.RealLeft) < (LeftNode.RadiusControl / 2) && Math.Abs(RightNode.RealTop - LeftNode.RealTop) < (LeftNode.RadiusControl / 2))
                {
                    if (NextSegment != null && PrevSegment != null)
                    {
                        PrevSegment.RightNode = NextSegment.LeftNode = RightNode;
                        PrevSegment.NextSegment = NextSegment;
                        NextSegment.PrevSegment = PrevSegment;
                    }
                    else if (PrevSegment != null)
                    {
                        PrevSegment.NextSegment = null;
                        PrevSegment.RightNode = RightNode;
                    }
                    else if (NextSegment != null)
                    {
                        NextSegment.PrevSegment = null;
                        NextSegment.LeftNode = RightNode;
                    }
                    else
                    {
                        return;
                    }
                    _collection.Shapes.Remove(LeftNode);
                    _collection.Shapes.Remove(this);
                    return;
                }
                #endregion Рассчет удаления ребер
           }
            Vector v1 = new Vector(RightNode.RealLeft - LeftNode.RealLeft, RightNode.RealTop - LeftNode.RealTop);
            HeightControl = v1.Length;
            Angle = Vector.AngleBetween(new Vector(1, 0), v1);
            
            innerUpdateParameter = true;
            if (RealTop != LeftNode.RealTop + (LeftNode.RadiusControl / 2) - WidthControl / 2)
            {
                System.Diagnostics.Debug.WriteLine(string.Format("было {0} стало {1}", LeftNode.RealTop, LeftNode.RealTop + (LeftNode.RadiusControl / 2) - WidthControl / 2));
                Top = LeftNode.RealTop + (LeftNode.RadiusControl / 2) - WidthControl / 2;
            }

            if (RealLeft != LeftNode.RealLeft + (LeftNode.RadiusControl / 2) )
            {
                System.Diagnostics.Debug.WriteLine(string.Format("было {0} стало {1}", LeftNode.RealLeft, LeftNode.RealLeft + (LeftNode.RadiusControl / 2)));
                Left = LeftNode.RealLeft + (LeftNode.RadiusControl / 2) ;
            }

            innerUpdateParameter = false;
            
        }

        public void Remove()
        { 
            SegmentViewModel current = this;
            while(current.PrevSegment != null)
            {
                current = current.PrevSegment;
            }

            while (current.NextSegment != null)
            {
                _collection.Shapes.Remove(current.LeftNode);
                current = current.NextSegment;
                _collection.Shapes.Remove(current.PrevSegment);
            }
            if (current.PrevSegment != null)
            {
                _collection.Shapes.Remove(current.PrevSegment);
            }
            _collection.Shapes.Remove(current.LeftNode);
            _collection.Shapes.Remove(current.RightNode);
            _collection.Shapes.Remove(current);
        }

        public void FullLineSelect()
        {
            SegmentViewModel current = this;
            while (current.PrevSegment != null)
            {
                current = current.PrevSegment;
            }

            while (current.NextSegment != null)
            {
                current.Selected = true;
                current.LeftNode.Selected = true;
                current = current.NextSegment;
            }
            current.Selected = true;
            current.LeftNode.Selected = true;
            current.RightNode.Selected = true;
        }
        
        public void FullLineUpdateTop(double increment)
        {
            if (innerUpdateParameter == true)
            {
                this.UpdateTop(increment);
                return;
            }
            innerUpdateParameter = true;
            SegmentViewModel current = this;
            while (current.PrevSegment != null)
            {
                current = current.PrevSegment;
            }

            while (current.NextSegment != null)
            {
                current.UpdateTop(increment);
                current.LeftNode.UpdateTop(increment);
                current = current.NextSegment;
            }
            current.UpdateTop(increment);
            current.LeftNode.UpdateTop(increment);
            current.RightNode.UpdateTop(increment);
            innerUpdateParameter = false;
        }

        public void FullLineUpdateLeft(double increment)
        {
            if (innerUpdateParameter == true)
            {
                this.UpdateLeft(increment);
                return;
            }
          //  System.Diagnostics.Debug.WriteLine(string.Format(" было {1} добавляем {0}", increment, this.RealLeft));

            if (Math.Abs(increment) > 1000)
            { 
            }
            innerUpdateParameter = true;
            SegmentViewModel current = this;
            while (current.PrevSegment != null)
            {
                current = current.PrevSegment;
            }

            while (current.NextSegment != null)
            {
                current.UpdateLeft(increment);
                current.LeftNode.UpdateLeft(increment);
                current = current.NextSegment;
            }
            current.UpdateLeft(increment);
            current.LeftNode.UpdateLeft(increment);
            current.RightNode.UpdateLeft(increment);
            innerUpdateParameter = false;
        }

        public override void UpdateLeft(double incrementLeft)
        {
            _left += incrementLeft;
            if (innerUpdateParameter == true)
            {
                OnPropertyChanged("Left");
                OnPropertyChanged("RealLeft");
            }
            else
            {
                innerUpdateParameter = true;
                OnPropertyChanged("Left");
                OnPropertyChanged("RealLeft");
                innerUpdateParameter = false;
            }
            
        }

        public override void UpdateTop(double incrementTop)
        {
            _top += incrementTop;
            if (innerUpdateParameter == true)
            {
                OnPropertyChanged("Top");
                OnPropertyChanged("RealTop");
            }
            else
            {
                innerUpdateParameter = true;
                OnPropertyChanged("Top");
                OnPropertyChanged("RealTop");
                innerUpdateParameter = false;
            }
        }

        public override bool IncludeInRegion(double x1, double y1, double x2, double y2)
        {
            return RightNode.IncludeInRegion(x1, y1, x2, y2) && LeftNode.IncludeInRegion(x1, y1, x2, y2);
        }
        #endregion  //Методы
    }
}
