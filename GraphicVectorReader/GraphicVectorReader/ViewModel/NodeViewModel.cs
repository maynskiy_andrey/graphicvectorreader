﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicVectorReader.ViewModel
{
    public class NodeViewModel: ShapeViewModel
    {
        #region Поля
        double _radiusControl;
        bool _isMoving;
        SegmentViewModel _segment;
        #endregion  //Поля

        #region Конструкторы
        public NodeViewModel(ShapesCollectionViewModel collection)
            : base(collection)
        { }
        #endregion  //Конструкторы

        #region Свойства
        public double RadiusControl
        {
            get { return _radiusControl; }
            set
            {
                if (_radiusControl != value)
                {
                    _radiusControl = value;
                    OnPropertyChanged("RadiusControl");
                }
            }
        }

        public bool IsMoving
        {
            get { return _isMoving; }
            set
            {
                if (_isMoving != value)
                {
                    _isMoving = value;
                    OnPropertyChanged("IsMoving");
                }
            }
        }

        private double _leftUse;
        public double  TopUse
        {
            get { return Top - _radiusControl/2; }
            set
            {
                if (Top - _radiusControl / 2  != value)
                {
                    Top = value + _radiusControl / 2;
                    OnPropertyChanged("TopUse");
                }
            }
        }

        int _zIndex = 100;
        public int ZIndex
        {
            get { return _zIndex; }
            set
            {
                if (_zIndex != value)
                {
                    _zIndex = value;
                    OnPropertyChanged("ZIndex");
                }
            }
        }

        public double LeftUse
        {
            get { return Left + _radiusControl / 2; }
            set
            {
                if (Left + _radiusControl / 2 != value)
                {

                    Left = value - _radiusControl / 2;
                    OnPropertyChanged("LeftUse");
                }
            }
        }

        public SegmentViewModel Segment
        {
            get { return _segment; }
            set { _segment = value; }
        }
        #endregion  //Свойства

        #region Методы
        public override bool IncludeInRegion(double x1, double y1, double x2, double y2)
        {
            return (Left) <= x2 && (Left) >= x1 && (Top) <= y2 && (Top) >= y1;
        }
        #endregion  //Методы
    }
}