﻿using System;
using System.ComponentModel;
using System.Diagnostics;

namespace GraphicVectorReader.ViewModel
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        private string _displayName;
        public event PropertyChangedEventHandler PropertyChanged;

        public string DisplayName
        {
            get { return _displayName; }
            set { _displayName = value; }
        }

        protected void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        [Conditional("DEBUG")]
        [DebuggerStepThrough]
        public void VerifyPropertyName(string propertyName)
        {
            if (TypeDescriptor.GetProperties(this)[propertyName] == null)
            {
                string msg = "Ошибка в названии свойства: " + propertyName;

                if (this.ThrowOnInvalidPropertyName)
                    throw new Exception(msg);
                else
                    Debug.Fail(msg);
            }
        }

        protected virtual bool ThrowOnInvalidPropertyName { get; private set; }
    }
}
