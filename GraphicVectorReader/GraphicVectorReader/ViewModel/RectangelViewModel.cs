﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using GraphicVectorReader.Properties;


namespace GraphicVectorReader.ViewModel
{
    public class RectangelViewModel : ShapeViewModel
    {
        #region Поля
        private double _width;
        private double _height;
        private double _anger;
        private Color _firstColor;
        private Color _secondColor;
        private bool _gradientColoring;
        #endregion

        #region Конструкторы
        public RectangelViewModel(ShapesCollectionViewModel collection)
            : base(collection)
        {
        }

        #endregion

        #region Свойства
        /// <summary>
        /// Ширина
        /// </summary>
        public double WidthControl
        {
            get { return _width; }
            set
            {
                if (_width != value)
                {
                    _width = value;
                    OnPropertyChanged("WidthControl");
                    OnPropertyChanged("CenterX");
                }
            }
        }

        /// <summary>
        /// Высота
        /// </summary>
        public double HeightControl
        {
            get { return _height; }
            set 
            { 
                if(_height != value)
                {
                    _height = value;
                    OnPropertyChanged("HeightControl");
                    OnPropertyChanged("CenterY");
                }
            }
        }

        /// <summary>
        /// Угол поворота
        /// </summary>
        public double Angle
        {
            get { return _anger; }
            set 
            {
                if (_anger != value)
                {
                    _anger = value;
                    OnPropertyChanged("Angle");
                }
            }
        }

        public double CenterX
        {
            get { return _width / 2; }
        }

        public double CenterY
        {
            get { return _height / 2; }
        }

       

        public Color FirstColor
        {
            get { return _firstColor; }
            set
            {
                if (_firstColor != value)
                {
                    _firstColor = value;
                    OnPropertyChanged("FirstColor");
                }
            }
        }

        public Color SecondColor
        {
            get { return _secondColor; }
            set 
            {
                if (_secondColor != value)
                {
                    _secondColor = value;
                    OnPropertyChanged("SecondColor");
                }
            }
        }

        public bool GradientColoring
        {
            get { return _gradientColoring; }
            set
            {
                if (_gradientColoring != value)
                {
                    _gradientColoring = value;
                    OnPropertyChanged("GradientColoring");
                    OnPropertyChanged("SingleColoring");
                }
            }
        }

        public bool SingleColoring
        {
            get { return !_gradientColoring; }
            set
            {
                if (_gradientColoring = value)
                {
                    _gradientColoring = !value;
                    OnPropertyChanged("GradientColoring");
                    OnPropertyChanged("SingleColoring");
                }
            }
        }
        #endregion

        public override bool IncludeInRegion(double x1, double y1, double x2, double y2)
        {
            return (CenterX+Left) <= x2 && (CenterX + Left) >= x1 && (CenterY + Top) <= y2 && (CenterY + Top) >= y1;
        }
    }
}
