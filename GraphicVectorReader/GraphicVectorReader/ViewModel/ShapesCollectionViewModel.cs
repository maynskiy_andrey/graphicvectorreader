﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Data;
using GraphicVectorReader.Properties;
using System.Windows.Shapes;
using System;
using System.Windows.Input;
using System.Windows.Media;


namespace GraphicVectorReader.ViewModel
{
    public class ShapesCollectionViewModel: ViewModelBase
    {
        #region Поля и константы
        double defaultLeftRectangle = 100;
        double defaultTopRectangle = 100;
        double defaultWidthRectangle = 50;
        double defaultHeightRectangle = 70;
        double defaultAngleRectangle = 10;
        Color defaultFirstColorRectangle = Colors.Red;
        Color defaultSecondColorRectangle = Colors.Yellow;

        ObservableCollection<ShapeViewModel> _shapes;
        private bool _multiSelectInProcess;
        ShapeViewModel _selectedShape;
        bool _isSelectedRectangle;
        RectangelViewModel _selectedRectangle;
        bool _isSelectedLine;
        SegmentViewModel _selectedLine;
        ICommand _createRectangleCommand;
        ICommand _deleteRectangleCommand;
        ICommand _createLineCommands;
        double _leftCommonCenter;
        double _topCommonCenter;
        #endregion  //Поля и константы

        #region Конструкторы
        public ShapesCollectionViewModel()
        {
            _shapes = new ObservableCollection<ShapeViewModel>();
            _isSelectedRectangle = false;
        }
        #endregion  //Конструкторы

        #region Свойства
        public ObservableCollection<ShapeViewModel> Shapes
        {
            get { return _shapes; }
            set
            {
                if (_shapes != value)
                {
                    _shapes = value;
                    OnPropertyChanged("Shapes");
                }
            }
        }

        public bool IsSelectedRectangle
        {
            get { return _isSelectedRectangle; }
            set
            {
                if (_isSelectedRectangle != value)
                {
                    _isSelectedRectangle = value;
                    OnPropertyChanged("IsSelectedRectangle");
                }
            }
        }

        public RectangelViewModel SelectedRectangle
        {
            get { return _selectedRectangle; }
            set
            {
                if (_selectedRectangle != value)
                {
                    _selectedRectangle = value;
                    OnPropertyChanged("SelectedRectangle");
                    IsSelectedRectangle = _selectedRectangle != null;
                }
            }
        }

        public bool IsSelectedLine
        {
            get { return _isSelectedLine; }
            set
            {
                if (_isSelectedLine != value)
                {
                    _isSelectedLine = value;
                    OnPropertyChanged("IsSelectedLine");
                }
            }
        }

        public SegmentViewModel SelectedLine
        {
            get { return _selectedLine; }
            set
            {
                if (_selectedLine != value)
                {
                    _selectedLine = value;
                    OnPropertyChanged("SelectedLine");
                    IsSelectedLine = _selectedLine != null;
                }
            }
        }

        public void ClearSelected()
        {
            foreach (ShapeViewModel shape in Shapes)
            {
                shape.Selected = false;
            }
        }

        public ShapeViewModel SelectedShape
        {
            get { return _selectedShape; }
            set
            {
                if (_selectedShape != value)
                {
                    _selectedShape = value;
                    SelectedRectangle = null;
                    SelectedLine = null;
                    
                    if (_multiSelectInProcess == false)
                    {
                        foreach (ShapeViewModel shape in _shapes)
                        {
                            if (shape != _selectedShape)
                            {
                                shape.Selected = false;
                            }
                        }

                        if (_selectedShape is RectangelViewModel)
                        {
                            SelectedRectangle = (RectangelViewModel)_selectedShape;
                        }
                        else if (_selectedShape is SegmentViewModel)
                        {
                            SelectedLine = (SegmentViewModel)_selectedShape;
                        }
                        else if (_selectedShape is NodeViewModel)
                        {
                            SelectedLine = (_selectedShape as NodeViewModel).Segment;
                        }
                    }
                    OnPropertyChanged("SelectedShape");
                }
            }
        }

        public double LeftCommonCenter
        {
            get { return _leftCommonCenter; }
            set
            {
                if (_leftCommonCenter != value)
                {
                    _leftCommonCenter = value;
                    OnPropertyChanged("LeftCommonCenter");
                }
            }
        }

        public double TopCommonCenter
        {
            get { return _topCommonCenter; }
            set
            {
                if (_topCommonCenter != value)
                {
                    _topCommonCenter = value;
                    OnPropertyChanged("TopCommonCenter");
                }
            }
        }
        #endregion

        #region Команды
        public ICommand CreateRectangleCommand
        {
            get
            {
                if (_createRectangleCommand == null)
                    _createRectangleCommand = new RelayCommand(param => this.CreateRectangle());

                return _createRectangleCommand;
            }
        }

        public ICommand CreateLineCommands
        {
            get
            {
                if (_createLineCommands == null)
                    _createLineCommands = new RelayCommand(param => this.CreateLine());

                return _createLineCommands;
            }
        }

        public ICommand DeleteRectangleCommand
        {
            get
            {
                if (_deleteRectangleCommand == null)
                    _deleteRectangleCommand = new RelayCommand(param => this.DeleteRectangle());

                return _deleteRectangleCommand;
            }
        }
        #endregion  //Команды

        #region Методы
        void CreateRectangle()
        {
            RectangelViewModel rect = new RectangelViewModel(this);
            rect.Left = defaultLeftRectangle - LeftCommonCenter;
            rect.Top = defaultTopRectangle - TopCommonCenter;
            rect.WidthControl = defaultWidthRectangle;
            rect.HeightControl = defaultHeightRectangle;
            rect.Angle = defaultAngleRectangle;
            rect.Selected = true;
            rect.FirstColor = defaultFirstColorRectangle;
            rect.SecondColor = defaultSecondColorRectangle;
            SelectedRectangle = rect;
        }

        void DeleteRectangle()
        {
            if (IsSelectedRectangle)
            {
                Shapes.Remove(SelectedShape);
                IsSelectedRectangle = false;
            }
            else if (IsSelectedLine)
            {
                SelectedLine.Remove();
                Shapes.Remove(SelectedShape);
                IsSelectedLine = false;
            }
            else
            {
                for (int i = Shapes.Count -1; i >= 0; --i)
                {
                    if (Shapes[i].Selected)
                    {
                        Shapes.Remove(Shapes[i]);
                    }
                }
            }
        }

        void CreateLine()
        {
            SegmentViewModel newSegment = new SegmentViewModel(this);
        }

        public void MultiselectByPoints(double x1,double x2, double y1, double y2)
        {
            SelectedShape = null;
            _multiSelectInProcess = true;
            foreach (ShapeViewModel share in Shapes)
            {
                if (share.IncludeInRegion(x1, y1, x2, y2))
                {
                    share.Selected = true;
                    if (share is SegmentViewModel)
                    {
                        (share as SegmentViewModel).FullLineSelect();
                    }
                }
            }
            _multiSelectInProcess = false;
        }

        public void SaveToStream(SaveLoad.BinaryWriterEx s)
        {
            List<RectangelViewModel> rectangles = new List<RectangelViewModel>();

            List<SegmentViewModel[]> lines = new List<SegmentViewModel[]>();
            foreach (ShapeViewModel shape in _shapes)
            {
                if (shape is RectangelViewModel)
                {
                    rectangles.Add(shape as RectangelViewModel);
                }
                else if (shape is SegmentViewModel)
                {
                    if((shape as SegmentViewModel).PrevSegment == null)
                    {
                        SegmentViewModel segment = shape as SegmentViewModel;
                        List<SegmentViewModel> newLine = new List<SegmentViewModel>();
                        newLine.Add(segment);
                        if (segment.NextSegment != null)
                        {
                            SegmentViewModel currentSegment = segment.NextSegment;
                            newLine.Add(currentSegment);
                            while (currentSegment.NextSegment != null)
                            {
                                currentSegment = currentSegment.NextSegment;
                                newLine.Add(currentSegment);
                            }
                        }
                        lines.Add(newLine.ToArray());
                    }
                }
            }

            s.Write(rectangles.Count);
            foreach (RectangelViewModel rectangle in rectangles)
            {
                s.Write(rectangle.RealTop);
                s.Write(rectangle.RealLeft);
                s.Write(rectangle.WidthControl);
                s.Write(rectangle.HeightControl);
                s.Write(rectangle.Angle);
                s.Write(Convertor.ColorToInt(rectangle.FirstColor));
                s.Write(Convertor.ColorToInt(rectangle.SecondColor));
                s.Write(rectangle.GradientColoring);
            }

            s.Write(lines.Count);
            foreach (SegmentViewModel[] line in lines)
            {
                s.Write(Convertor.ColorToInt(line[0].Color));
                s.Write(line[0].WidthControl);

                s.Write(line.Count());
                s.Write(line[0].LeftNode.RealTop);
                s.Write(line[0].LeftNode.RealLeft);
                for (int i = 0; i < line.Count(); i++)
                {
                    s.Write(line[i].RightNode.RealTop);
                    s.Write(line[i].RightNode.RealLeft);
                }
            }
            
        }

        public void LoadToStream(SaveLoad.BinaryReaderEx s)
        {
            int countElement = s.ReadInt32();
            for (int i = 0; i < countElement; i++)
            {
                RectangelViewModel rectangle = new RectangelViewModel(this);
                rectangle.Top = s.ReadDouble();
                rectangle.Left = s.ReadDouble();
                rectangle.WidthControl = s.ReadDouble();
                rectangle.HeightControl = s.ReadDouble();
                rectangle.Angle = s.ReadDouble();
                rectangle.FirstColor = Convertor.ConvertIntToColor(s.ReadInt32());
                rectangle.SecondColor = Convertor.ConvertIntToColor(s.ReadInt32());
                rectangle.GradientColoring = s.ReadBoolean();
            }

            countElement = s.ReadInt32();
            for (int i = 0; i < countElement; i++)
            {
                SegmentViewModel segment = new SegmentViewModel(this);
                segment.Color = Convertor.ConvertIntToColor(s.ReadInt32());
                segment.WidthControl = s.ReadDouble();
                
                int countNodes = s.ReadInt32();
                NodeViewModel leftNode = new NodeViewModel(this);
                leftNode.Segment = segment;
                segment.LeftNode.Top = s.ReadDouble();
                segment.LeftNode.Left = s.ReadDouble();

                for (int j = 0; j < countNodes; j++)
                {
                    if(j == 0)
                    {
                        segment.RightNode.Top = s.ReadDouble();
                        segment.RightNode.Left = s.ReadDouble();
                    } else
                    { 
                        NodeViewModel rightNode = new NodeViewModel(this);
                        rightNode.Top = s.ReadDouble();
                        rightNode.Left = s.ReadDouble();
                        

                        SegmentViewModel nextSegment = new SegmentViewModel(segment, rightNode, this);
                        rightNode.Segment = nextSegment;
                        segment = null;
                        segment = nextSegment;
                    }
                }
            }
        }
        #endregion  //Методы
    }
}
