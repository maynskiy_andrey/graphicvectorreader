﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Data;
using GraphicVectorReader.Properties;
using System.Windows.Shapes;
using System;
using System.Windows.Input;
using System.Windows;


namespace GraphicVectorReader.ViewModel
{
    public class MainWindowViewModel: ViewModelBase
    {
        #region Поля и константы
        double defaultSizeMultiselectRectangle = 2;

        ShapesCollectionViewModel _shapesCollection;
        ICommand _clickCanvasCommand;
        ICommand _unClickCanvasCommand;
        ICommand _moveMouseCanvasCommand;
        ICommand _openDocumentCommand;
        ICommand _saveDocumentCommand;
        private double _heigthMultiSelectRectangle;
        private double _widthMultiSelectRectangle;
        private double _leftMultiSelectRectangle;
        private double _topMultiSelectRectangle;
        private bool _multiSelectedInProcess = false;
        private bool _changeCenterInProcess = false;
        private double _leftStart;
        private double _topStart;
        private double _leftCenterStart;
        private double _topCenterStart;
        #endregion

        #region Конструкторы
        public MainWindowViewModel()
        {
            _shapesCollection = new ShapesCollectionViewModel();
        }
        #endregion

        #region Свойства
        public ShapesCollectionViewModel ShapesCollection
        {
            get { return _shapesCollection; }
            set
            {
                if (_shapesCollection != value)
                {
                    _shapesCollection = value;
                    OnPropertyChanged("ShapesCollection");
                }
            }
        }
        #endregion

        #region Закрытие
        public event EventHandler RequestClose;

        void OnRequestClose()
        {
            EventHandler handler = this.RequestClose;
            if (handler != null)
                handler(this, EventArgs.Empty);
        }
        #endregion //Закрытие
        
        #region Команды
        public ICommand ClickCanvasCommand
        {
            get
            {
                if (_clickCanvasCommand == null)
                    _clickCanvasCommand = new RelayCommand(param => this.ClickCanvas(param));

                return _clickCanvasCommand;
            }
        }

        public ICommand UnClickCanvasCommand
        {
            get
            {
                if (_unClickCanvasCommand == null)
                    _unClickCanvasCommand = new RelayCommand(param => this.UnClickCanvas(param));

                return _unClickCanvasCommand;
            }
        }

        public ICommand MoveMouseCanvasCommand
        {
            get
            {
                if (_moveMouseCanvasCommand == null)
                    _moveMouseCanvasCommand = new RelayCommand(param => this.MoveMouseCanvas(param));

                return _moveMouseCanvasCommand;
            }
        }

        public ICommand SaveDocumentCommand
        {
            get
            {
                if (_saveDocumentCommand == null)
                    _saveDocumentCommand = new RelayCommand(param => this.SaveDocument());

                return _saveDocumentCommand;
            }
        }

        public ICommand OpenDocumentCommand
        {
            get
            {
                if (_openDocumentCommand == null)
                    _openDocumentCommand = new RelayCommand(param => this.LoadDocument());

                return _openDocumentCommand;
            }
        }
        #endregion //Команды

        #region Методы
        public bool MultiSelectedInProcess
        {
            get { return _multiSelectedInProcess; }
            set
            {
                if (_multiSelectedInProcess != value)
                {
                    _multiSelectedInProcess = value;
                    OnPropertyChanged("MultiSelectedInProcess");
                }
            }
        }

        public double HeigthMultiSelectRectangle
        {
            get { return _heigthMultiSelectRectangle; }
            set 
            {
                if (_heigthMultiSelectRectangle != value)
                {
                    _heigthMultiSelectRectangle = value;
                    OnPropertyChanged("HeigthMultiSelectRectangle");
                }
            }
        }

        public double WidthMultiSelectRectangle
        {
            get { return _widthMultiSelectRectangle; }
            set
            {
                if (_widthMultiSelectRectangle != value)
                {
                    _widthMultiSelectRectangle = value;
                    OnPropertyChanged("WidthMultiSelectRectangle");
                }
            }
        }

        public double TopMultiSelectRectangle
        {
            get { return _topMultiSelectRectangle; }
            set
            {
                if (_topMultiSelectRectangle != value)
                {
                    _topMultiSelectRectangle = value;
                    OnPropertyChanged("TopMultiSelectRectangle");
                }
            }
        }

        public double LeftMultiSelectRectangle
        {
            get { return _leftMultiSelectRectangle; }
            set
            {
                if (_leftMultiSelectRectangle != value)
                {
                    _leftMultiSelectRectangle = value;
                    OnPropertyChanged("LeftMultiSelectRectangle");
                }
            }
        }
        
        public void ClickCanvas(object param)
        {
            ShapesCollection.ClearSelected();
            Point curPosition = Mouse.GetPosition((System.Windows.Controls.Canvas)param);
            _leftStart = curPosition.X;
            _topStart = curPosition.Y;

            if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
            {
                MultiSelectedInProcess = true;
                TopMultiSelectRectangle = curPosition.Y;
                LeftMultiSelectRectangle = curPosition.X;
                WidthMultiSelectRectangle = defaultSizeMultiselectRectangle;
                HeigthMultiSelectRectangle = defaultSizeMultiselectRectangle;
            }
            else
            {
                _changeCenterInProcess = true;
                _leftCenterStart = ShapesCollection.LeftCommonCenter;
                _topCenterStart = ShapesCollection.TopCommonCenter;
            }
        }

        public void UnClickCanvas(object param)
        {
            _changeCenterInProcess = false;
            if (MultiSelectedInProcess == true)
            {
                MultiSelectedInProcess = false;

                double x1, x2, y1, y2;
                Point curPosition = Mouse.GetPosition((System.Windows.Controls.Canvas)param);

                if (curPosition.X - _leftStart > 0)
                {
                    x1 = _leftStart;
                    x2 = curPosition.X;
                }
                else
                {
                    x1 = curPosition.X;
                    x2 = _leftStart;
                }

                if (curPosition.Y - _topStart > 0)
                {
                    y1 = _topStart;
                    y2 = curPosition.Y;
                }
                else
                {
                    y1 = curPosition.Y;
                    y2 = _topStart;
                }
                _shapesCollection.MultiselectByPoints(x1, x2, y1, y2);
            }
        }

        public void MoveMouseCanvas(object param)
        {
            Point curPosition = Mouse.GetPosition((System.Windows.Controls.Canvas)param);

            if (MultiSelectedInProcess == true)
            {
                if (curPosition.X - _leftStart > 0)
                {
                    LeftMultiSelectRectangle = _leftStart;
                    WidthMultiSelectRectangle = curPosition.X - _leftStart;
                }
                else
                {
                    LeftMultiSelectRectangle = curPosition.X;
                    WidthMultiSelectRectangle = _leftStart - curPosition.X;
                }

                if (curPosition.Y - _topStart > 0)
                {
                    TopMultiSelectRectangle = _topStart;
                    HeigthMultiSelectRectangle = curPosition.Y - TopMultiSelectRectangle;
                }
                else
                {
                    TopMultiSelectRectangle = curPosition.Y;
                    HeigthMultiSelectRectangle = _topStart - curPosition.Y; 
                }
            }
            else if (_changeCenterInProcess == true)
            {
                ShapesCollection.LeftCommonCenter = _leftCenterStart + curPosition.X - _leftStart;
                ShapesCollection.TopCommonCenter = _topCenterStart + curPosition.Y - _topStart;
            }
        }

        public void SaveDocument()
        {
            SaveLoad.LoadSaveHelper.Save(ShapesCollection);
        }

        public void LoadDocument()
        {
            SaveLoad.LoadSaveHelper.Load(ShapesCollection);
        }
        #endregion
    }
}
