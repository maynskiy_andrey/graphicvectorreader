﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GraphicVectorReader.Control
{
    /// <summary>
    /// Interaction logic for NodeControl.xaml
    /// </summary>
    public partial class NodeControl : ChangePositionControl
    {
        public NodeControl()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty RadiusControlProperty = DependencyProperty.Register("RadiusControl", typeof(double), typeof(NodeControl));
        public double RadiusControl
        {
            get { return (double)GetValue(RadiusControlProperty); }
            set { this.SetValue(RadiusControlProperty, value); }
        }


        
    }
}
