﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GraphicVectorReader.Control
{
    public class ChangePositionControl: UserControl
    {
        #region Поля
        double startX;
        double startY;
        #endregion  //Поля

        #region Конструкторы
        static ChangePositionControl()
        {
        }
        #endregion  //Конструкторы

        #region Перегрузки событий
        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            IsMoving = true;
            double localLeft = Canvas.GetLeft(this);
            double localTop = Canvas.GetTop(this);
            if (double.IsNaN(localLeft)) localLeft = 0;
            if (double.IsNaN(localTop)) localTop = 0;
            var currentMousePosition = e.GetPosition(this);
            startX = currentMousePosition.X;
            startY = currentMousePosition.Y;
            this.CaptureMouse();
            base.OnMouseDown(e);
            Selected = true;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (IsMoving)
            {
                double localLeft = Canvas.GetLeft(this);
                double localTop = Canvas.GetTop(this);
                
                var currentMousePosition = e.GetPosition(this);

               // System.Diagnostics.Debug.WriteLine(string.Format("X Было {0} добавляем {1} Y Было {2} добавляем {3}", localLeft,
                    //currentMousePosition.X - startX - LeftCenter,
                    //localTop, currentMousePosition.Y - startY - TopCenter));

                Canvas.SetLeft(this, localLeft + currentMousePosition.X - startX - LeftCenter);
                Canvas.SetTop(this, localTop + currentMousePosition.Y - startY - TopCenter );
            }
            base.OnMouseMove(e);
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            this.ReleaseMouseCapture();
            IsMoving = false;
            base.OnMouseUp(e);
        }
        #endregion  //Перегрузки событий

        #region Свойства
        public static readonly DependencyProperty SelectedProperty = DependencyProperty.Register("Selected", typeof(bool), typeof(ChangePositionControl));
        public bool Selected
        {
            get { return (bool)GetValue(SelectedProperty); }
            set { this.SetValue(SelectedProperty, value); }
        }

        public static readonly DependencyProperty IsMovingProperty = DependencyProperty.Register("IsMoving", typeof(bool), typeof(ChangePositionControl));
        public bool IsMoving
        {
            get { return (bool)GetValue(IsMovingProperty); }
            set { this.SetValue(IsMovingProperty, value); }
        }

        public static readonly DependencyProperty TopCenterProperty = DependencyProperty.Register("TopCenter", typeof(double), typeof(ChangePositionControl));
        public double TopCenter
        {
            get { return (double)GetValue(TopCenterProperty); }
            set { this.SetValue(TopCenterProperty, value); }
        }

        public static readonly DependencyProperty LeftCenterProperty = DependencyProperty.Register("LeftCenter", typeof(double), typeof(ChangePositionControl));
        public double LeftCenter
        {
            get { return (double)GetValue(LeftCenterProperty); }
            set { this.SetValue(LeftCenterProperty, value); }
        }
        #endregion
    }
}
