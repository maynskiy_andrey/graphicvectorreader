﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GraphicVectorReader.Control
{
    /// <summary>
    /// Interaction logic for RidControl.xaml
    /// </summary>
    public partial class RidControl : ChangePositionControl
    {
        #region Конструкторы
        public RidControl()
        {
            InitializeComponent();
        }
        #endregion

        #region Перегрузка событий
        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            Selected = true;
            base.OnMouseDown(e);
        }
        #endregion //Перегрузка событий

        #region Свойства
        public static readonly DependencyProperty HeightControlProperty = DependencyProperty.Register("HeightControl", typeof(double), typeof(RidControl));
        public double HeightControl
        {
            get { return (double)GetValue(HeightControlProperty); }
            set { this.SetValue(HeightControlProperty, value); }
        }

        public static readonly DependencyProperty WidthControlProperty = DependencyProperty.Register("WidthControl", typeof(double), typeof(RidControl));
        public double WidthControl
        {
            get { return (double)GetValue(WidthControlProperty); }
            set { this.SetValue(WidthControlProperty, value); }
        }

        //public static readonly DependencyProperty SelectedProperty = DependencyProperty.Register("Selected", typeof(bool), typeof(RidControl));
        //public bool Selected
        //{
        //    get { return (bool)GetValue(SelectedProperty); }
        //    set { this.SetValue(SelectedProperty, value); }
        //}

        //public static readonly DependencyProperty TopCenterProperty = DependencyProperty.Register("TopCenter", typeof(double), typeof(RidControl));
        //public double TopCenter
        //{
        //    get { return (double)GetValue(TopCenterProperty); }
        //    set { this.SetValue(TopCenterProperty, value); }
        //}

        //public static readonly DependencyProperty LeftCenterProperty = DependencyProperty.Register("LeftCenter", typeof(double), typeof(RidControl));
        //public double LeftCenter
        //{
        //    get { return (double)GetValue(LeftCenterProperty); }
        //    set { this.SetValue(LeftCenterProperty, value); }
        //}

        public static readonly DependencyProperty ColorProperty = DependencyProperty.Register("Color", typeof(Color), typeof(RidControl));
        public Color Color
        {
            get { return (Color)GetValue(ColorProperty); }
            set { this.SetValue(ColorProperty  , value); }
        }
        #endregion
    }
}
