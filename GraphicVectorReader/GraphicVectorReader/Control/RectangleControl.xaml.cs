﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GraphicVectorReader.Control
{
    /// <summary>
    /// Interaction logic for RectangleControl.xaml
    /// </summary>
    public partial class RectangleControl : ChangePositionControl
    {
        public RectangleControl()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty HeightControlProperty = DependencyProperty.Register("HeightControl", typeof(double), typeof(RectangleControl));
        public double HeightControl
        {
            get { return (double)GetValue(HeightControlProperty); }
            set { this.SetValue(HeightControlProperty, value); }
        }

        public static readonly DependencyProperty WidthControlProperty = DependencyProperty.Register("WidthControl", typeof(double), typeof(RectangleControl));
        public double WidthControl
        {
            get { return (double)GetValue(WidthControlProperty); }
            set { this.SetValue(WidthControlProperty, value); }
        }

        public static readonly DependencyProperty FirstColorProperty = DependencyProperty.Register("FirstColor", typeof(Color), typeof(RectangleControl));
        public Color FirstColor
        {
            get { return (Color)GetValue(FirstColorProperty); }
            set { this.SetValue(FirstColorProperty, value); }
        }

        public static readonly DependencyProperty SecondColorProperty = DependencyProperty.Register("SecondColor", typeof(Color), typeof(RectangleControl));
        public Color SecondColor
        {
            get { return (Color)GetValue(SecondColorProperty); }
            set { this.SetValue(SecondColorProperty, value); }
        }

        public static readonly DependencyProperty GradientColoringProperty = DependencyProperty.Register("GradientColoring", typeof(bool), typeof(RectangleControl));
        public bool GradientColoring
        {
            get { return (bool)GetValue(GradientColoringProperty); }
            set { this.SetValue(GradientColoringProperty, value); }
        }
    }
}
