﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace GraphicVectorReader.SaveLoad
{
    public class BinaryWriterEx : BinaryWriter
    {
        public BinaryWriterEx(Stream output) : base(output) { }
        public override void Write(string value)
        {
            if (String.IsNullOrEmpty(value))
            {
                Write(0);
            }
            else
            {
                Write(value.Length);
                base.Write(value);
            }
        }
    }
    public class BinaryReaderEx : BinaryReader
    {
        public BinaryReaderEx(Stream input) : base(input) { }
        public override string ReadString()
        {
            if (0 < ReadInt32())
                return base.ReadString();
            else
                return String.Empty;
        }
    }
}
