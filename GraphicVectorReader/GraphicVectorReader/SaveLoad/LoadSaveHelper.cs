﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Threading;

namespace GraphicVectorReader.SaveLoad
{
    public class LoadSaveHelper
    {
        public static void Save(ViewModel.ShapesCollectionViewModel collection)
        {
            try
            {
                Microsoft.Win32.SaveFileDialog saveFialog = new Microsoft.Win32.SaveFileDialog();
                if (saveFialog.ShowDialog() == true)
                {
                    using (FileStream file = new FileStream(saveFialog.FileName, FileMode.OpenOrCreate))
                    {
                        BinaryWriterEx writer = new BinaryWriterEx(file);
                        collection.SaveToStream(writer);
                    }
                }
            }
            catch
            {
                MessageBox.Show("При сохранение произошла непредвиденная ошибка", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public static void Load(ViewModel.ShapesCollectionViewModel collection)
        {
            Microsoft.Win32.OpenFileDialog openDialog = new Microsoft.Win32.OpenFileDialog();
            if (openDialog.ShowDialog() == true)
            {
                using (FileStream file = new FileStream(openDialog.FileName, FileMode.Open))
                {
                    BinaryReaderEx reader = new BinaryReaderEx(file);
                    collection.LoadToStream(reader);
                }
            }
        }
    }
}
