﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace GraphicVectorReader
{
    public class ShapeTemplateSelector: DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, System.Windows.DependencyObject container)
        {
            FrameworkElement element = container as FrameworkElement;

            if (element != null && item != null)
            {
                if (item is ViewModel.RectangelViewModel)
                {
                    return element.FindResource("RectangleTemplate") as DataTemplate;
                }
                else if (item is ViewModel.NodeViewModel)
                {
                    return element.FindResource("NodeTemplate") as DataTemplate;
                }
                else if (item is ViewModel.SegmentViewModel)
                {
                    return element.FindResource("RibTemplate") as DataTemplate;
                } 

            }
            return base.SelectTemplate(item, container);
        }
    }
}
