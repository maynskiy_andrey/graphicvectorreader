﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows;
using System.Windows.Media;

namespace GraphicVectorReader
{
    public class BoolToVisibleConverter : IValueConverter
    {
        public static readonly BoolToVisibleConverter Instance = new BoolToVisibleConverter();
        public object Convert(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            bool visible = (bool)value;
            if (visible)
            {
                return Visibility.Visible;
            }
            else
            {
                return Visibility.Hidden;
            }
        }

        public object ConvertBack(object value, Type targetTypes, object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(typeof(int), typeof(Color))]
    public class IntToColorConverter : IValueConverter
    {
        public static readonly IntToColorConverter Instance = new IntToColorConverter();
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return new SolidColorBrush((Color)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int i = (int)((((Color)value).R << 16) | (((Color)value).G << 8) | ((Color)value).B);
            return i;
        }
    }

    public class Convertor
    {
        public static int ColorToInt(Color value)
        {
            int i = (int)((((Color)value).R << 16) | (((Color)value).G << 8) | ((Color)value).B);
            return i;
        }
        public static Color ConvertIntToColor(int value)
		{
			Color c = Color.FromRgb(
                    (byte)(((int)value >> 16 & 0xFF)),
                   (byte)(((int)value >> 8) & 0xFF),
                   (byte)(((int)value) & 0xFF));
			return c;
		}

    }
}
